// Import our Controllers
const postController = require('../controllers/postController')
const Posts = require("../models/Post");


function schema_routes_post(model, hide, required) {
  return {
    hide: hide,
    tags: ["API"],
    body: {
      type: "object",
      properties: model.schema.obj,
      required: required
    },
    response: {
      201: {
        description: "Created",
        type: "object",
        properties: {
          message: {
            type: "string"
          },
          data: {
            type: "string"
          }
        }
      },
      400: {
        description: "Fail request",
        type: "object",
        properties: {
          error: {
            type: "string"
          }
        }
      },
      500: {
        description: "Server error",
        type: "object",
        properties: {
          error: {
            type: "string"
          }
        }
      }
    }
  };
}



const routes = [
  {
    method: 'GET',
    url: '/api/posts',
    handler: postController.getPosts
  },
  {
    method: 'GET',
    url: '/api/posts/:id',
    handler: postController.getSinglePost
  },
  {
    method: 'POST',
    url: '/api/posts/:id',
    schema: schema_routes_post(Posts, false, ["title", "link", "marca"]),
    handler: postController.getSinglePost
  },
  {
    method: 'DELETE',
    url: '/api/posts/:id',
    handler: postController.deletePost
  }
]

module.exports = routes
