// Require the fastify framework and instantiate it
const fastify = require('fastify')({
  logger: true
})



// Require external modules
const mongoose = require('mongoose')

// Import Routes
const routes = require('./routes')

// Import Swagger Options
const swagger = require('./config/swagger')

// Register Swagger
fastify.register(require('fastify-swagger'), swagger.options)

fastify.register(require('fastify-cors'), {
  methods: ['GET', 'PUT', 'PATCH', 'POST', 'DELETE']
})
// Connect to DB
mongoose.connect('mongodb+srv://botFacebook:bc123456!@dadosface-aakyu.mongodb.net/agroprospects', { useNewUrlParser: true })
  .then(() => console.log('MongoDB connected...'))
  .catch(err => console.log(err))

// Loop over each route
routes.forEach((route, index) => {
  fastify.route(route)
})

// Run the server!
const start = async () => {
  try {
    await fastify.listen(3000, '0.0.0.0')
    fastify.swagger()
    fastify.log.info(`server listening on ${fastify.server.address().port}`)
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}
start()
